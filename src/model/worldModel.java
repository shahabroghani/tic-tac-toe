package model;

public class worldModel {
    private Board board;

    public worldModel() {
        this.board = new Board();
    }

    public Board getBoard() {
        return board;
    }

    public void executeAct(int index,piece piece){
        board.setColumn(index,piece);
    }
}
