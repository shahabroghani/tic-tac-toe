package model;

import java.util.Random;

public class AI extends player {
    public AI(String name) {
        super(name);
    }

    @Override
    public int getAct(worldModel worldModel) {
        Random random = new Random();
        return random.nextInt(9);
    }
}
