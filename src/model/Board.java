package model;

import java.util.HashMap;
import java.util.Map;

public class Board {
    private Map<Integer,column> columns = new HashMap<>();
   // private ArrayList<column> columns = new ArrayList<>();

    public Board() {
        columns.put(0,new column(0,false,null));
        columns.put(1,new column(1,false,null));
        columns.put(2,new column(2,false,null));
        columns.put(3,new column(3,false,null));
        columns.put(4,new column(4,false,null));
        columns.put(5,new column(5,false,null));
        columns.put(6,new column(6,false,null));
        columns.put(7,new column(7,false,null));
        columns.put(8,new column(8,false,null));
    }

    public column getColumn(int index) {
        return columns.get(index);
    }

    public void setColumn(int index,piece piece){
        columns.get(index).setFull(true);
        columns.get(index).setPiece(piece);
    }
}
