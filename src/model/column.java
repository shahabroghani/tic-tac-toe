package model;

public class column {
    private Integer pos;
    private Boolean full;
    private piece piece;

    public column(Integer pos, Boolean full,piece piece) {
        this.pos = pos;
        this.full = full;
        this.piece = piece;
    }

    public Integer getPos() {
        return pos;
    }

    public void setPos(Integer pos) {
        this.pos = pos;
    }

    public boolean getFull() {
        return full;
    }

    public void setFull(Boolean full) {
        this.full = full;
    }

    public model.piece getPiece() {
        return piece;
    }

    public void setPiece(piece piece) {
        this.piece = piece;
    }
}
