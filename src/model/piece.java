package model;

public class piece {
    private String color;
    private Boolean form;
    private Integer id;
    private Integer pos;

    public piece(String color, Boolean form,Integer id,Integer pos) {
        this.color = color;
        this.form = form;
        this.id = id;
        this.pos = pos;
    }

    public String getColor() {
        return color;
    }

    public Boolean getForm() {
        return form;
    }

    public Integer getId() {
        return id;
    }

    public Integer getPos() {
        return pos;
    }
}
