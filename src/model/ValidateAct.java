package model;

public class ValidateAct {
    public static boolean validate(worldModel worldModel,int index,int count){
        if (count>0 && index>=0 && index<=8) {
            return !worldModel.getBoard().getColumn(index).getFull();
        }
        return false;
    }
}
