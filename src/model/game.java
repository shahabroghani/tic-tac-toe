package model;

import java.util.ArrayList;
import java.util.Scanner;

public class game {
    private player player1;
    private player player2;
    private ArrayList<piece> player1Pieces = new ArrayList<>();
    private ArrayList<piece> player2Pieces = new ArrayList<>();
    private worldModel worldModel;
    private Boolean turn;
    private Integer player1Count;
    private Integer player2Count;
    public game() {
        player1 = createPlayer();
        player2 = secondPlayer();
        initPieces();
        worldModel = new worldModel();
        turn = false;
        player1Count = player1Pieces.size();
        player2Count = player2Pieces.size();
        run();
    }

    private void initPieces() {
        for (int i=0 ; i<5 ; i++){
            player1Pieces.add(new piece("#FF0000",true,1,null));
            player2Pieces.add(new piece("#0000FF",false,2,null));
        }
    }

    private void showBoard() {
        for (int i=0 ; i<9 ; i++){
            column column = worldModel.getBoard().getColumn(i);
            if (i%3==0){
                System.out.println();
            }
            if (column.getFull()){
                System.out.print(column.getPiece().getId()+" ");
            }else {
                System.out.print("# ");
            }
        }
        System.out.println();
    }

    private void run(){
        while (!evaluate()){
            showBoard();
            if (turn){
                int act = player1.getAct(worldModel);
                if (ValidateAct.validate(worldModel,act,player1Count)){
                    worldModel.executeAct(act,player1Pieces.get(player1Count-1));
                    turn = false;
                    player1Count--;
                }else {
                    System.out.println("Wrong Action!!");
                }
            }else {
                int act = player2.getAct(worldModel);
                if (ValidateAct.validate(worldModel,act,player2Count)){
                    worldModel.executeAct(act,player2Pieces.get(player2Count-1));
                    turn = true;
                    player2Count--;
                }else {
                    System.out.println("Wrong Action!!");
                }
            }
        }
        if (turn){
            System.out.println(player2.getName()+" wine!!");
        }else {
            System.out.println(player1.getName()+" wine!!");
        }
    }

    private player secondPlayer() {
        System.out.println("Select second player: ");
        System.out.println("1. human");
        System.out.println("2. AI");
        Scanner scanner = new Scanner(System.in);
        int secondPlayer = scanner.nextInt();
        player player;
        switch (secondPlayer){
            case 2:
                player = new AI("AI");
                break;
            case 1:
                player = createPlayer();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + secondPlayer);
        }
        return player;
    }

    private player createPlayer() {
        System.out.println("Enter your name: ");
        Scanner scanner = new Scanner(System.in);
        return new human(scanner.next());
    }

    private boolean evaluate(){
        Integer p1;
        Integer p2;
        Integer p3;
        Integer p4;
        Integer p5;
        Integer p6;
        Integer p7;
        Integer p8;
        Integer p9;
        if (worldModel.getBoard().getColumn(0).getPiece()!=null) {
            p1 = worldModel.getBoard().getColumn(0).getPiece().getId();
        }else {
            p1 = 0;
        }
        if (worldModel.getBoard().getColumn(1).getPiece()!=null) {
            p2 = worldModel.getBoard().getColumn(1).getPiece().getId();
        }else {
            p2 = 0;
        }
        if (worldModel.getBoard().getColumn(2).getPiece()!=null) {
            p3 = worldModel.getBoard().getColumn(2).getPiece().getId();
        }else {
            p3 = 0;
        }
        if (worldModel.getBoard().getColumn(3).getPiece()!=null) {
            p4 = worldModel.getBoard().getColumn(3).getPiece().getId();
        }else {
            p4 = 0;
        }
        if (worldModel.getBoard().getColumn(4).getPiece()!=null) {
            p5 = worldModel.getBoard().getColumn(4).getPiece().getId();
        }else {
            p5 = 0;
        }
        if (worldModel.getBoard().getColumn(5).getPiece()!=null) {
            p6 = worldModel.getBoard().getColumn(5).getPiece().getId();
        }else {
            p6 = 0;
        }
        if (worldModel.getBoard().getColumn(6).getPiece()!=null) {
            p7 = worldModel.getBoard().getColumn(6).getPiece().getId();
        }else {
            p7 = 0;
        }
        if (worldModel.getBoard().getColumn(7).getPiece()!=null) {
            p8 = worldModel.getBoard().getColumn(7).getPiece().getId();
        }else {
            p8 = 0;
        }
        if (worldModel.getBoard().getColumn(8).getPiece()!=null) {
            p9 = worldModel.getBoard().getColumn(8).getPiece().getId();
        }else {
            p9 = 0;
        }
        if (p1.equals(p2) && p1.equals(p3) && p1!=0){
            return true;
        }else if (p4.equals(p5) && p4.equals(p6) && p4!=0){
            return true;
        }else if (p7.equals(p8) && p7.equals(p9) && p7!=0){
            return true;
        }else if (p1.equals(p4) && p1.equals(p7) && p1!=0){
            return true;
        }else if (p2.equals(p5) && p2.equals(p8) && p2!=0){
            return true;
        }else if (p3.equals(p6) && p3.equals(p9) && p3!=0){
            return true;
        }else if (p1.equals(p5) && p1.equals(p9)&& p1!=0){
            return true;
        }else if (p3.equals(p5) && p3.equals(p7)&& p3!=0){
            return true;
        }
        return false;
    }
}
